@ECHO OFF

cd _R
R CMD BATCH --no-save --no-restore run.R
R CMD BATCH --no-save --no-restore tail.R
type tail.txt
del tail.txt
del tail.Rout
cd ..
REM echo knitting is done

call make html

del _build\html\_sources\rst\*.txt
rem copy rst\Rcode\*.R  _build\html\_sources\rst\*.txt /Y > NUL

xcopy rst\Rcode\*.R  _build\html\_sources\rst\*.txt /Y /Q


call make latex
cd _build/latex
pdflatex RSpatial.tex
xcopy *.pdf  ..\html\_sources\rst\*.pdf /Y /Q
cd ../..


:end
