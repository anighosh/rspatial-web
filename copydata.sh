#!/bin/bash
#TODO convert this to R or python so it's cross platform
sections=("intr" "cases" "sdm" "analysis" "spatial" "sphere")


# For each sub chapter knit
for i in "${sections[@]}"
do
  datadir=build/html/${i}/data
  mkdir $datadir
  echo "cp source/${i}/_R/data/* ${datadir}/"
  cp source/${i}/_R/data/* ${datadir}/
done
echo "Copying data is done"
