@ECHO OFF

cd _R
R CMD BATCH --no-save --no-restore run.R
R CMD BATCH --no-save --no-restore tail.R
type tail.txt
del tail.txt
del tail.Rout
cd ..
REM echo knitting is done

REM xcopy _R\data\*.*  rst\data\*.* /Y /Q

call make html

del _build\html\_sources\rst\*.txt
rem copy rst\Rcode\*.R  _build\html\_sources\rst\*.txt /Y > NUL

xcopy rst\Rcode\*.R  _build\html\_sources\rst\*.txt /Y /Q
REM xcopy rst\data\*.*  _build\html\data\*.* /Y /Q

:end
